<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="buysell">
                <form action="sell.php" method="POST" id = "sellform">
                    <div class="form-group">
                        <label for="sell">Sell Stocks</label>
                        <select class="form-control" name="sell">
                            <option disabled = "disabled" selected = "true">Select Symbol</option>
                            <?php foreach($stocks as $stock): ?>
                                <option name ="<?= $stock ?>"><?= $stock ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <?php if(isset($sellerror)): ?>
                        <div id = "sellerror">
                            <p>No symbol was selected</p>
                        </div>
                    <?php endif; ?>    
                    <div class="form-group">
                        <input type="submit"  name = "sellbutton" value="Sell It!" class = "btn btn-primary">
                    </div>

                </form>

            </div>

        </div>

    </div>

</div>
