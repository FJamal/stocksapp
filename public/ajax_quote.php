<?php
    //addin $_GET["symbol"] tpo url
    $url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={$_GET["symbol"]}&interval=1min&apikey=INF9154MRHQTFELA&datatype=csv";

    //opening the csv file
    $handle = fopen($url, "r");

    if($handle == false)
    {
        print("could not open the csv data file");
    }

    $rows = [];

    $counter = 0;
    while($data = fgetcsv($handle,","))
    {

        array_push($rows, $data);

        $counter += 1;

        if($counter == 4)
        {
            break;
        }
    }

    //closing the handle
    fclose($handle);

    $price = number_format($rows[1][4], 2, ".", "");

    //setting up header tell browser what type of response the file cr8s
    header("Content-type: application/json");
    print(json_encode($price));
?>
