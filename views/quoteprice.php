<!--load Charts API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php if(isset($quotePriceError)): ?>
                <p id = "quotePrice">Invalid Stock Symbol</p>
            <?php else: ?>
                <p id = "quotePrice" company = "<?= $company ?>" name = "<?= $symbol ?>">The price for <?= $symbol ?> (<?= $company ?>) is $<?= $price ?></p>
            <?php endif; ?>

        </div>
    </div>

    <!--for graphs-->
    <?php if(!isset($quotePriceError)): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="graphlist">
                    <ul id ="graphlist">
                        <li><button class= "btn btn-default" type="button" name="button">15 Days</button></li>
                        <li><button class= "btn btn-default" type="button" name="button">12 Months</button></li>
                        <li><button class= "btn btn-default" type="button" name="button">5 Years</button></li>
                        <li><button class ="btn btn-default" type= "button" name = "button">10 Years</button></li>
                    </ul>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <div id ="linechart">

                </div>

            </div>
        </div>
    <?php endif; ?>
</div>


<?php if(!isset($quotePriceError)): ?>
    <!--load charts api only when a valid symbol was submitted-->
    <script src = "/js/priceajax.js"></script>
<?php endif; ?>
