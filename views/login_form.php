
        <div class="loginpanel">
            <div class="row">
                <div class="col-md-4">
                    <h4>stock app</h4>
                </div>
                <div class="col-md-8">
                    <form class="form-inline" action="login.php" method="POST" id= "log">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder= "Username" name = "username">

                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class = "form-control" placeholder="password">
                        </div>
                        <input type="submit" name="login" value="Log In" class="btn btn-success">
                        <!--for login errors-->
                        <?php if(isset($loginerror)): ?>
                            <p class = "error"><?= $logerror ?></p>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="registerpanel">
            <form class="form-horizontal" action="register.php" method="POST" id ="registerform">

                <h3 class = "reg">Register</h3>
                <div class = "regbody">
                    <div class="form-group">
                        <div class = "col-sm-10">
                            <input type="text" name="fullname" placeholder="Full Name" class = "form-control" id ="fullname">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class = "col-sm-10">
                            <input type="text" name="username" placeholder="Username" class = "form-control" id = "username">

                        </div>
                    </div>
                    <div class="form-group">
                        <div class = "col-sm-10">
                            <input type="password" id ="password" name="password" placeholder="Password" class = "form-control">

                        </div>
                    </div>
                    <div class="form-group">
                        <div class = "col-sm-10">
                            <input type="password" id = "confirmation" name="confirmation" placeholder="Confirm Password" class = "form-control">
                            <!--To display error msg that comes from server validation-->
                            <?php if(isset($reg_error_flag)): ?>
                                <p class = "regerror" id = "confirmationerror">
                                    <?= $errormsg ?>
                                </p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class = "registerbutton">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="submit" class="btn btn-success" value = "Register" id = "submit">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
