<?php

    ini_set("display_errors", true);
    error_reporting(E_ALL);

    session_start();
    
    //require helpers functions
    require("helpers.php");

    if(!in_array($_SERVER["PHP_SELF"], ["/login.php", "/register.php"]))
    {

        if(empty($_SESSION["id"]))
        {
            redirect("login.php");
        }
    }
 ?>
