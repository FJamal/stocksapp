<?php
    //configuration
    require("../includes/config.php");

    //if user visits via GET redirect it to login.php
    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        redirect("login.php");
    }

    //if visiting via POST
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /*print("<pre>");
        print_r($_POST);
        print("</pre>");*/
        //saving $post variables to avoid long names
        $name = $_POST["fullname"];
        $username = $_POST["username"];
        $password = $_POST["password"];
        $confirmation = $_POST["confirmation"];

        //validation
        if(empty($name) || empty($username) || empty($password) || empty($confirmation))
        {
            $errorflag = true;
            $errormessage = "Please fill all the fields";

            //loading the login form again with the error msg
            render("login_form.php", ["reg_error_flag" => $errorflag, "errormsg" => $errormessage]);
        }
        elseif(strlen($username) < 6)
        {
            //if username is less than 6 chars
            $errorflag = true;
            $errormessage = "Username must be atleast 6 letters long";

            //loading the login form again with the error msg
            render("login_form.php", ["reg_error_flag" => $errorflag, "errormsg" => $errormessage]);
        }
        else if(strlen($password) < 6 )
        {
            //if username is less than 6 chars
            $errorflag = true;
            $errormessage = "password must be atleast 6 letters long";

            //loading the login form again with the error msg
            render("login_form.php", ["reg_error_flag" => $errorflag, "errormsg" => $errormessage]);
        }
        elseif($password != $confirmation)
        {
            //if username is less than 6 chars
            $errorflag = true;
            $errormessage = "passwords submitted did not match";

            //loading the login form again with the error msg
            render("login_form.php", ["reg_error_flag" => $errorflag, "errormsg" => $errormessage]);
        }
        else
        {
            //hashing the password
            $password = password_hash($password, PASSWORD_DEFAULT);



            //preparing statement
            $sql = $dbh->prepare("INSERT INTO users (full_name, password, username, cash) VALUES (:name, :password, :username, '10000.0000')");

            //binding values
            $sql->bindValue(":name", $name);
            $sql->bindValue(":password", $password);
            $sql->bindValue(":username", $username);

            //executing
            $result = $sql->execute();

            //checking if it returned false for duplicate username
            if($result == false)
            {
                //if result was 1 or that means that username already exsists
                $errorflag = true;
                $errormessage = "username selected already exsists";

                render("login_form.php", ["reg_error_flag" => $errorflag, "errormsg" => $errormessage]);
            }
            else
            {
                $id;
                //storing session id and taking user to homepage
                $rows = $dbh->query("SELECT * FROM users WHERE username = '{$username}'");

                foreach($rows as $row)
                {
                    $id = $row["id"];
                }

                $_SESSION["id"] = $id;

                //loading homepage redirecting to index.php
                redirect("index.php");
            }
        }

    }
?>
