var fullname = document.getElementById("fullname");
var username = document.getElementById("username");
var password = document.getElementById("password");
var confirmation = document.getElementById("confirmation");
var submit = document.getElementById("submit");
var form = document.getElementById("registerform");

//event handler to display tip when user focuses on username that it must be 6 letters long
username.onfocus = function ()
{
    //if error message is there
    var errormsg = document.getElementById("usernameerror");

    //getting usernames parent
    var parent = username.parentNode;

    //remove error msg
    if(errormsg)
    {
        parent.removeChild(errormsg);
    }


    //creating a new p tag for tip
    var p = document.createElement("p");
    var msg = document.createTextNode("Username must be atleast 6 letters long");

    // adding the text to p tag
    p.appendChild(msg);

    p.className = "tip";

    // adding created p to its parent
    parent.appendChild(p);

};

//event handler to remove tip once the user leaves the username form
username.onblur = function ()
{
    //get the p tag using class name and removing the tip
    var p = document.getElementsByClassName("tip");

    var parent = p[0].parentNode;

    parent.removeChild(p[0]);

    var user = username.value;
    console.log(user.length);
    //checking if user actually typed anything
    if(user.length !== 0)
    {
        if(user.length < 6)
        {
            //if length is less than 6 display error msg
            var ptag = document.createElement("p");
            var msg = document.createTextNode("Username was less than 6 letters long");

            ptag.appendChild(msg);

            ptag.className = "regerror";

            //giving this p tag an id also so that it can be removed when user corrects error
            ptag.setAttribute("id", "usernameerror");

            parent.appendChild(ptag);

            submit.setAttribute("disabled", "disabled");
        }
        else
        {
            //if it passed the letter limits test enable the submit button
            submit.disabled = false;
        }
    }

};


//event handler on password field to give tip abt the min length of password
password.onfocus = function ()
{
    //update:remove password match error, naturally user wud enter the password
    //again if he sees the serror so remove msg once user comes to this field
    var confirmationError = document.getElementById("confirmationerror");
    var parent2 = confirmation.parentNode;

    //if any error was already displaying remove it
    var error = document.getElementById("passworderror");

    //getting parent of password input
    var parent = password.parentNode;
    console.log(confirmationError);
    //removing confirmation error
    if(confirmationError)
    {
        parent2.removeChild(confirmationError);
    }
    //removing error
    if(error)
    {
        parent.removeChild(error);
    }

    //creating a p tag for tip
    var p = document.createElement("p");
    var msg = document.createTextNode("password must be atleast 6 letters long");

    //joining
    p.appendChild(msg);

    p.className= "tip";

    parent.appendChild(p);
};

// To remove the tip from password field when user leaves that field
password.onblur = function ()
{
    //getting the p tag using classname
    var p = document.getElementsByClassName("tip");
    var parent = p[0].parentNode;

    parent.removeChild(p[0]);

    //getting passwords value
    var pass = password.value;

    //checking if lenght of password was indeed >=6
    if(pass.length !== 0)
    {
        if(pass.length < 6)
        {
            //if length is less than 6 display error msg
            var ptag = document.createElement("p");
            var msg = document.createTextNode("password was less than 6 letters long");

            ptag.appendChild(msg);

            ptag.className = "regerror";

            //giving this p tag an id also so that it can be removed when user corrects error
            ptag.setAttribute("id", "passworderror");

            parent.appendChild(ptag);

            submit.setAttribute("disabled", "disabled");
        }
        else
        {
            submit.disabled = false;
        }
    }
};

//event handler to check if password matches, can only be done when form submitted since its the last input
form.onsubmit = function (e)
{
    //checking if confirmation error was already displaying
    var error = document.getElementById("confirmationerror");

    var parent = confirmation.parentNode;

    //if error is already displaying remove it
    if(error)
    {
        parent.removeChild(error);
    }

    //getting values of both passwords
    var value1 = password.value;
    var value2 = confirmation.value;

    if(fullname.value == ""|| fullname.value == null || username.value == "" || username.value == null || password.value == "" || password.value == null || confirmation.value == "" || confirmation.value == null)
    {
        var p = document.createElement("p");
        var msg = document.createTextNode("please fill all the fields");

        p.appendChild(msg);

        p.className = "regerror";

        //giving it an id so that it can be removed once error is corrected
        p.setAttribute("id", "confirmationerror");

        parent.appendChild(p);

        e.preventDefault();
    }
    else if(value1 != value2)
    {
        var p = document.createElement("p");
        var msg = document.createTextNode("passwords do not match");

        p.appendChild(msg);

        p.className = "regerror";

        //giving it an id so that it can be removed once error is corrected
        p.setAttribute("id", "confirmationerror");

        parent.appendChild(p);

        e.preventDefault();

    }
};
