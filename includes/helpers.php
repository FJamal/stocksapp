<?php

//connecting to db
$dsn = "mysql:host=localhost;dbname=stocksapp";

	try
	{
		$dbh = new PDO($dsn,"root","baadshah");
	}
	catch (PDOException $e)
	{
		echo "connection failed :" . $e->getMessage();
	}


/*function to load the viewpage and its variables */
function render($view, $values = [])
{
    if(file_exists("../views/{$view}"))
    {
        //extract the values passed to this page
        extract($values);
        require("../views/header.php");
        require("../views/{$view}");
        require("../views/footer.php");
    }
    else
    {
        trigger_error("invalid view = {$view}", E_USER_ERROR);
    }
}

function redirect($page)
{
	/* Redirect to a different page in the current directory that was requested */
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$extra = $page;
	header("Location: http://$host$uri/$extra");
	exit;
}


/*function to load the viewpage and its variables */
function render2($view, $values = [])
{
    if(file_exists("../views/{$view}"))
    {
        //extract the values passed to this page
        extract($values);
        require("../views/header2.php");
        require("../views/{$view}");
        require("../views/footer2.php");
    }
    else
    {
        trigger_error("invalid view = {$view}", E_USER_ERROR);
    }
}

/*function that gets quotes of the stock symbol
according to documentation alphavantage the api's close value is the
stocks latest price which is $rows[1][4] $rows[0] been the headings
for the csv file.... reutrns false on empty symbol field or invalid symbol*/
function getquote($symbol)
{
	//if quote field was filled
	if(!empty($symbol))
	{
		//$symbol = $_POST["symbol"];

		$url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={$symbol}&interval=1min&apikey=INF9154MRHQTFELA&datatype=csv";

		//opening the csv file
		$handle = fopen($url, "r");

		if($handle == false)
		{
			print("could not open the csv data file");
		}

		$rows = [];

		//counter to break while so that it stops after 4 iteration to decrease loading time
		$counter = 0;
		while($data = fgetcsv($handle,","))
		{

			array_push($rows, $data);
			$counter += 1;

			//breaking to decrease loading time that reads the entire csv file
			if($counter == 4)
			{
				break;
			}
		}

		//closing the handle
		fclose($handle);



		if(count($rows[0]) == 1)
		{
			return false;
		}
		else
		{
			$price = number_format($rows[1][4], 2, ".", "");
			return $price;
		}
	}
	else
	{
		return false;
	}
}


function logout()
{
	//unset the sessions
	$_SESSION = [];

	//expires the cookie
	if(!empty(session_name()))
	{
		setcookie(session_name(), "", time() - 36000);
	}

	//destroy sessions
	session_destroy();
}

?>
