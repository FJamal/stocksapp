<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("login_form.php");
    }
    else if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $username = $_POST["username"];
        $password = $_POST["password"];

        if(empty($username) || empty($password))
        {
            $errorflag = true;
            $msg = "Please fill all the fields";

            render("login_form.php", ["loginerror" => $errorflag, "logerror" => $msg]);
        }
        elseif(!empty($username) && !empty($password))
        {
            //checking if username is in db
            $sql = $dbh->prepare("SELECT * FROM users WHERE username = :username");
            $sql->bindValue(":username", $username);
            $sql->execute();

            //getting that row
            $row = $sql->fetch(PDO::FETCH_ASSOC);

            //if row was not empty
            if($row)
            {
                //check if passwords matched
                if(password_verify($password, $row["password"]))
                {
                    $_SESSION["id"] = $row["id"];

                    redirect("index.php");
                }
                else
                {
                    $flag = true;
                    $msg = "Invalid Password";

                    render("login_form.php", ["loginerror" => $flag, "logerror"=> $msg]);
                }
            }
            else
            {
                $flag = true;
                $msg = "Invalid Username or Password";

                render("login_form.php", ["loginerror" => $flag, "logerror"=> $msg]);
            }

        }
    }

 ?>
