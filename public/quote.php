<?php
    //configuration
    require("../includes/config.php");

    //diableing visits via get and loading homepage instead
    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        redirect("index.php");
    }
    else if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $symbol = $_POST["symbol"];

        $quote = getquote($symbol);



        //converting it to uppercase
        $symbol = strtoupper($symbol);

        //fetching name of symbol from db
        $sql = $dbh->prepare("SELECT name FROM companies WHERE symbol = :symbol");
        $sql->bindValue(":symbol", $symbol);
        $sql->execute();

        $row = $sql->fetch(PDO::FETCH_ASSOC);

        $company = $row["name"];


        if($quote != false)
        {
            render2("quoteprice.php", ["symbol" => $symbol, "price" => $quote, "company" => $company]);
        }
        else
        {
            $flag = true;
            render2("quoteprice.php", ["quotePriceError" => $flag]);
        }
    }
?>
