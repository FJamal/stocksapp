<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        $stocks = [];
        //getting the stocks symbol the user owns
        $rows = $dbh->query("SELECT symbol FROM portfolios WHERE user_id = {$_SESSION["id"]}");

        foreach($rows as $row)
        {
            array_push($stocks, $row["symbol"]);
        }
        //print_r($stocks);
        render2("sell_form.php", ["stocks" => $stocks]);
    }
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        /*validation if $_POST["sellbutton"] is only set that means user just
        just pressed sell button without selecting any symbol*/
        if(count($_POST) == 1)
        {
            $stocks = [];
            //getting the stocks symbol the user owns
            $rows = $dbh->query("SELECT symbol FROM portfolios WHERE user_id = {$_SESSION["id"]}");

            foreach($rows as $row)
            {
                array_push($stocks, $row["symbol"]);
            }
            //print_r($stocks);
            render2("sell_form.php", ["stocks" => $stocks, "sellerror" => true]);
        }
        else
        {
            //get the current price of the symbol
            $symbol = $_POST["sell"];
            $price = getquote($symbol);

            //get the number of shares user has
            $sql = $dbh->prepare("SELECT shares, company FROM portfolios WHERE user_id = :userid AND symbol = :symbol ");
            $sql->bindValue(":userid", $_SESSION["id"]);
            $sql->bindValue(":symbol", $symbol);
            $sql->execute();

            $result = $sql->fetch(PDO::FETCH_ASSOC);
            $shares = $result["shares"];
            $company_name = $result["company"];

            //cash user will get on selling
            $cash = number_format($price * $shares, 2 , ".", "");

            /*delete the row in portfolios
            with symbol of $_POST["sell"] and seesion id of this user with
            transactions*/
            $dbh->beginTransaction();
            $sql = $dbh->prepare("DELETE FROM portfolios WHERE user_id = :userid AND symbol= :symbol");
            $sql->bindValue(":userid", $_SESSION["id"]);
            $sql->bindValue(":symbol", $symbol);
            $sql->execute();

            //updating the cash
            $sql = $dbh->exec("UPDATE users SET cash = cash + {$cash} WHERE id = {$_SESSION["id"]}");

            //inserting into history
            $sql= $dbh->prepare("INSERT INTO history (user_id, action, date_time, symbol, company, shares, price, total) VALUES (:userid, \"SELL\", Now(), :symbol, :company, :shares, :price, :total)");
            $sql->bindValue(":userid", $_SESSION["id"]);
            $sql->bindValue(":symbol", $symbol);
            $sql->bindValue(":company", $company_name);
            $sql->bindValue(":shares", $shares);
            $sql->bindValue(":price", $price);
            $sql->bindValue(":total",$cash);
            $sql->execute();

            $dbh->commit();


            redirect("sell.php");
        }
    }
?>
