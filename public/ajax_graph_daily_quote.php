<?php
    $url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={$_GET["symbol"]}&apikey=INF9154MRHQTFELA&datatype=csv";

    $handle = fopen($url, "r");

    if($handle == false)
    {
        print("could not open csv file");
    }

    $rows = [];
    $counter = 0;
    while($data = fgetcsv($handle, ","))
    {
        array_push($rows, $data);
        $counter += 1;

        //condition to get the data for the last 15 days
        if($counter == 16)
        {
            break;
        }
    }

    /*iterating over rows $rows[0] is headings according to documentation so
    skip that and $rows[1][0] is date and $rows[0][4] is closing price
    for that day*/
    $jsontosend = [];
    $count = 0;
    foreach($rows as $row)
    {
        //skipping $rows[0]
        if($count == 0)
        {
            $count += 1;
            continue;
        }
        $jsontosend [] = ["date" => $row[0],
                          "price" => number_format($row[4], 2 , ".", "")];
    }

    header("Content-type: application/json");
    print(json_encode($jsontosend));
?>
