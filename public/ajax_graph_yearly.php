<?php
    $url = "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol={$_GET["symbol"]}&apikey=INF9154MRHQTFELA&datatype=csv";

    //flag to determine if data required is for 5 years or 10 years in ajax
    $years = $_GET["years"];
    $flag;
    if($years == 5)
    {
        $flag = 63;
    }
    else if($years == 10)
    {
        $flag = 123;
    }

    $handle = fopen($url, "r");

    if($handle == false)
    {
        header("Content-type: application/json");
        print(json_encode("Error"));
    }

    $rows = [];

    //counter to note number of iterations
    $counter = 0;
    while($data = fgetcsv($handle,","))
    {
        //to skip first 2 iterations
        if($counter < 2)
        {
            $counter += 1;
            continue;
        }

        array_push($rows, $data);

        $counter += 1;

        /*to break while for 5 yrs as first 2 rows are irelevent
        so for 5 yrs it would have to be 12 * 5 + 2*/
        if($counter == $flag)
        {
            break;
        }
    }

    //associative array to be send as json data
    $jsontosend = [];

    foreach($rows as $row)
    {
        $jsontosend [] = ["date" => $row[0], "price" => number_format($row[4], 2, ".", "")];
    }

    header("Content-type: application/json");
    print(json_encode($jsontosend));

?>
