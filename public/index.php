<?php
    //configuration
    require("../includes/config.php");

    //getting how much cash user has and his name
    $sql = $dbh->prepare("SELECT cash, full_name FROM users WHERE id = :id");
    $sql->bindValue(":id", $_SESSION["id"]);
    $sql->execute();

    $row = $sql->fetch(PDO::FETCH_ASSOC);

    $cash = number_format($row["cash"], 2, ".", "");
    $name = $row["full_name"];

    //getting all the shares the user owns
    $sql = $dbh->prepare("SELECT shares, symbol, company FROM portfolios WHERE user_id = :userid");
    $sql->bindValue(":userid", $_SESSION["id"]);
    $sql->execute();

    $portfolios = [];

    while($row = $sql->fetch(PDO::FETCH_ASSOC))
    {
        // getting the current price of the symbol
        $price = getquote($row["symbol"]);

        //calculating the total for its stocks
        $total = $price * $row["shares"];
        $total = number_format($total, 2, ".", "");

        //inserting all the data in portfolios array
        $portfolios [] = ["name" => $row["company"],
                        "symbol" => $row["symbol"],
                        "price" => $price,
                        "shares" => $row["shares"],
                        "total" => $total];
    }


    render2("homepage.php", ["cashleft" => $cash, "name" => $name, "portfolios" => $portfolios]);
    /*require("../views/header2.php");
    require("../views/footer2.php");*/
?>
