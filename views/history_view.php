<div class="container">
    <h3><?= $name ?>'s Transaction History</h3>
    <?php if(isset($data) && !empty($data)): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="history">
                    <table class ="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Action</th>
                                <th>Symbol</th>
                                <th>Company</th>
                                <th>Price</th>
                                <th>Shares</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data as $row): ?>
                                <tr>
                                    <td><?= $row["date"] ?></td>
                                    <td><?= $row["time"] ?></td>
                                    <td><?= $row["action"] ?></td>
                                    <td><?= $row["symbol"] ?></td>
                                    <td><?= $row["company"] ?></td>
                                    <td>$<?= $row["price"] ?></td>
                                    <td><?= $row["shares"] ?></td>
                                    <td>$<?= $row["total"] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php else: ?>
        <p id = "quotePrice">You have no transactions to show</p>
    <?php endif; ?>
</div>
