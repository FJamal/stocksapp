<div class="container">
    <h3><?= $name ?>'s Stocks Portfolio</h3>

    <div class ="row">

        <div class= "col-lg-12">
            <div id = "custom">
                <table class = "table table-striped">
                    <thead>
                        <tr>
                            <th>Company</th>
                            <th>Symbol</th>
                            <th>Price</th>
                            <th>Shares</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($portfolios)): ?>
                            <?php foreach($portfolios as $portfolio): ?>
                                <tr>
                                    <td><?= $portfolio["name"] ?></td>
                                    <td><?= $portfolio["symbol"] ?></td>
                                    <td>$<?= $portfolio["price"] ?></td>
                                    <td><?= $portfolio["shares"] ?></td>
                                    <td>$ <?= $portfolio["total"]?></td>
                                </tr>
                            <?php endforeach ; ?>
                        <?php endif; ?>
                        <tr>
                            <th colspan = "4" id = "cashleft">Cash Left</th>
                            <th >$<?= $cashleft ?></th>

                        </tr>
                    </tbody>

                </table>
            </div>
        </div>

    </div>

    <?php if(!empty($portfolios)): ?>
        <div class="row">
            <div class="col-lg-12">
                <div id = "piechart">

                </div>

            </div>
        </div>
    <?php endif; ?>

</div>

<!--adding piechart script and everything-->
<?php if(!empty($portfolios)): ?>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
            ["Symbol", "Shares"],
            <?php foreach($portfolios as $portfolio): ?>
                ["<?= $portfolio["symbol"] ?>", <?= $portfolio["shares"] ?>],
            <?php endforeach; ?>
            ]);

            var options = {
                title: 'Your Portfolio',
                is3D: true,
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
<?php endif; ?>
