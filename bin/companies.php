<?php
    //configuration
    require("../includes/helpers.php");

    //creating db
    $dbh->query("CREATE TABLE companies (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    symbol VARCHAR(20),
    name VARCHAR(255),
    INDEX(symbol,name)
    )ENGINE = INNODB");

    // opening csv file
    $handle = fopen("companylist.csv", "r");

    if($handle == false)
    {
        print("could not open file");
    }

    $rows = [];
    $count = 0;
    while($data = fgetcsv($handle, ","))
    {
        //escaping the first entry as its the headings
        if($count == 0)
        {
            $count += 1;
            continue;

        }
        array_push($rows, $data);
        


    }

    $datafordb = [];

    foreach ($rows as $row)
    {
        $datafordb [] = ["symbol" => $row[0],
                         "name" => $row[1]];
    }

    print_r($datafordb);

    $sql = $dbh->prepare("INSERT INTO companies(symbol, name) VALUES(:symbol, :name)");

    //entering into db
    foreach ($datafordb as $entry)
    {
        $sql->bindValue(":symbol", $entry["symbol"]);
        $sql->bindValue(":name", $entry["name"]);
        $sql->execute();
    }
?>
