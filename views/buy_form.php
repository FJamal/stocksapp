<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class = "buysell">
                <form action="buy.php" method="post">
                    <div class="form-group">
                        <label for="Buy">Buy Stocks</label>
                        <input type="text" name="symbol" placeholder = "Stock Symbol" class ="form-control">
                    </div>
                    <div class="form-group">
                        <label for="Quantity">Quantity</label>
                        <input type="text" class = "form-control" name="quantity" placeholder="Number of Stocks to buy">
                    </div>
                    <?php if(isset($validationerror)): ?>
                        <div class="buyerror">
                            <p><?= $message ?></p>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <input type="submit" class = "btn btn-primary" value="Buy It!">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
