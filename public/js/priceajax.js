var xhr = null;

//setting up timeinterval for 10 secs
window.setInterval(getprice, 40000);

//getting the location of list for graph options
var graphlist = document.getElementById("graphlist");

//Load the Visualization API and the corechart package.
google.charts.load('45', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(graphfor15days);

//stting up click event handler on the entire list
graphlist.onclick = graphoptions;





/*Function that displays chart for 5/10 years takes in
a number as an argument that is passed to php file
to determine the data that is wanted*/
function yearly(years)
{
    //remove any graphs
    removegraph();

    var xhr;
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if(xhr == false)
    {
        alert("javascript not supported");
    }

    //getting symbol thru p tag
    var p = document.getElementById("quotePrice");
    var symbol = p.getAttribute("name");

    var url = "ajax_graph_yearly.php?symbol=" + symbol + "&years=" + years;

    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                var jsondata = JSON.parse(xhr.responseText);

                //api configuration
                 var data = new google.visualization.DataTable();
                data.addColumn("date", "Date");
                data.addColumn("number", "Price");

                //filling data table with rows gotten via ajax
                for(var i = jsondata.length - 1; i >=0; i --)
                {
                    var dateinpieces = jsondata[i].date.split("-");

                    var year = Number(dateinpieces[0]);
                    var month = Number(dateinpieces[1]);
                    var day = Number(dateinpieces[2]);

                    data.addRows([[new Date(year, month -1 , day), Number(jsondata[i].price)]]);
                }

                var title = "Stock's Price For Last " + years + " Years";
                var options = {"title": title,
                                vAxes:{0: {title: "Price (US $)"}}
                                };

                var chart = new google.visualization.LineChart(document.getElementById("linechart"));

                chart.draw(data, options);
            }
        }
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}





/*function that displays the chart for last 15days when page is loaded*/
function graphfor15days()
{
    //removing existing graph
    removegraph();

    var xhr;
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if(xhr == false)
    {
        alert("Javascript not supported");
    }



    //getting the symbol from p tag
    var p = document.getElementById("quotePrice");
    var symbol = p.getAttribute("name");

    var url = "ajax_graph_daily_quote.php?symbol=" + symbol;

    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                var jsondata = JSON.parse(xhr.responseText);


                /*adding data from json into chart and creating chart*/
                var data = new google.visualization.DataTable();

                //adding columns
                data.addColumn("date", "Date");
                data.addColumn("number", "Price");

                //filling rows and starting from last to improve chart visually
                for(var i = jsondata.length - 1; i >= 0; i --)
                {
                    /* breaking date 2017-m-d as a string to sperate type
                    casted year month day variables so that they could be used
                    in rows as Date opbject for chart*/
                    var dateinpieces = jsondata[i].date.split("-");


                    var year = Number(dateinpieces[0]);
                    var month = Number(dateinpieces[1]);
                    var day = Number(dateinpieces[2]);



                    /*adding the columns and in date month is minus one
                    cuz javscript has months from 0-11*/
                    data.addRows([[new Date(year, month - 1, day), Number(jsondata[i].price)]]);
                }

                var options = {"title": "Stock's Price For Last 15 Days",
                                vAxes: {0: {title: "Price (US $)" }}
                                };

                var chart = new google.visualization.LineChart(document.getElementById("linechart"));

                chart.draw(data, options);
            }
        }
    }
    xhr.open("GET", url, true);
    xhr.send(null);
}




/*Function to display 12 months price */
function graphfor12months()
{
    //removing existing chart
    removegraph();

    var xhr
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if(xhr == false)
    {
        alert("javascript not supported");
    }

    //getting the symbol of current stock from the p tag
    var p = document.getElementById("quotePrice");
    var symbol = p.getAttribute("name");

    var url = "ajax_graph_monthly_quote.php?symbol=" + symbol;

    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                var jsondata = JSON.parse(xhr.responseText);


                // creating and filling columns for google chart API
                var data = new google.visualization.DataTable();

                //first arg = type .....2nd arg = name
                data.addColumn("date", "Date");
                data.addColumn("number", "Price");

                for(var i = jsondata.length - 1; i >= 0; i --)
                {
                    //splitting date with - dash mark
                    var dateinpieces = jsondata[i].date.split("-");

                    var year = Number(dateinpieces[0]);
                    var month = Number(dateinpieces[1]);
                    var day = Number(dateinpieces[2]);

                    //adding data to rows...month is minus 1 cuz javascript has month (0-11);
                    data.addRows([[new Date(year, month - 1, day), Number(jsondata[i].price)]]);
                }

                //options for api
                var options = {"title": "Stock's Price For The Last 12 Months",
                                vAxes: { 0 : {title : "Price (US $)"}}};

                var chart = new google.visualization.LineChart(document.getElementById("linechart"));

                chart.draw(data, options);
            }
        }
    }

    xhr.open("GET", url, true);
    xhr.send(null);
}




/*Function that removes the existing graph if there is any*/
function removegraph()
{
    var graph = document.getElementById("linechart");
    graph.innerHTML = "";
}




function graphoptions(e)
{
    if(!e)
    {
        e = window.event;
    }

    var target = e.target || e.srcElement;

    // getting the value of the button clicked
    var value = target.textContent;

    if(value == "12 Months")
    {
        //load graph for 12 months
        graphfor12months();
    }
    else if(value == "15 Days")
    {
        graphfor15days();
    }
    else if(value == "5 Years")
    {
        yearly(5);
    }
    else if(value == "10 Years")
    {
        yearly(10);
    }
}


/*function that updates the stock price via ajax */
function getprice()
{
    //instantiaing xmlhttprequest
    try
    {
        xhr = new XMLHttpRequest();
    }
    catch (e)
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    //xhr = new XMLHttpRequest();
    if(xhr == false)
    {
        alert("Javascript not supported");
        return;
    }

    //get name of symbol through name attribute of p tag thats displaying price
    var p = document.getElementById("quotePrice");
    var symbol = p.getAttribute("name");

    //storing company name from attribute of p wirh id quotePrice
    var companyname = p.getAttribute("company");


    var url = "ajax_quote.php?symbol=" + symbol;



    xhr.onreadystatechange = function ()
    {
        if(xhr.readyState == 4)
        {
            if(xhr.status == 200)
            {
                var price = JSON.parse(xhr.responseText);


                //msg to display
                var msg = "The price for ";
                msg += symbol;
                msg += " ( ";
                msg += companyname;
                msg += " )";
                msg += " is $";
                msg += price;

                

                //substituting the msg in the p tag
                p.textContent = msg;
            }
        }
    }

    xhr.open("GET", url, true);
    xhr.send(null);


}
