<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        //gtting users name
        $sql = $dbh->prepare("SELECT full_name FROM users WHERE id = :id");
        $sql->bindValue(":id", $_SESSION["id"]);
        $sql->execute();
        $result = $sql->fetch(PDO::FETCH_ASSOC);
        $name = $result["full_name"];

        //getting rows from history db that belong to this user
        $rows = $dbh->query("SELECT * FROM history WHERE user_id = {$_SESSION["id"]}");

        $data = [];

        foreach($rows as $row)
        {
            $data [] = ["action" => $row["action"],
                        "symbol" => $row["symbol"],
                        "company" => $row["company"],
                        "shares" => $row["shares"],
                        "price" => number_format($row["price"], 2, ".", ""),
                        "total" => number_format($row["total"], 2, ".", ""),
                        "date" => $row["date_time"]];
        }

        //formating date and time of each data array
        $counter = 0;
        foreach($data as $row)
        {
            $temp = $row["date"];
            $exploded = explode(" ", $temp);

            $date_broken = $exploded[0];
            $time = $exploded[1];

            //saving the new date
            $data[$counter]["date"] = $date_broken;

            //adding a new key for time
            $data[$counter]["time"] = $time;

            $counter += 1;
        }

        render2("history_view.php", ["name" => $name, "data" => $data]);
    }
?>
