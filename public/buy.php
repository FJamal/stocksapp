<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render2("buy_form.php");
    }

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $symbol = $_POST["symbol"];
        $symbol = strtoupper($symbol);
        $quantity; //=$_POST["quantity"];
        if(is_numeric($_POST["quantity"]))
        {
            $quantity = (int)$_POST["quantity"];
        }
        else
        {
            $quantity = $_POST["quantity"];
        }


        // validation
        if(empty($_POST["symbol"]) && empty($_POST["quantity"]))
        {
            $flag = true;
            $msg = "Please fill all the fields";

            render2("buy_form.php", ["validationerror" => $flag, "message" => $msg]);
        }
        else
        {

            //if quantity is not an int or less than 1
            if(!is_int($quantity) || $quantity < 1)
            {
                $flag = true;
                $msg = "Invalid symbol/quantity";

                render2("buy_form.php", ["validationerror" => $flag, "message" => $msg]);
            }
            else
            {
                //checking symbol
                $symbol_price = getquote($symbol);

                if($symbol_price == false)
                {
                    $flag = true;
                    $msg = "Invalid symbol/quantity";

                    render2("buy_form.php", ["validationerror" => $flag, "message" => $msg]);
                }
                else
                {
                    //getting cash of user
                    $sql = $dbh->prepare("SELECT cash FROM users WHERE id = :userid");
                    $sql->bindValue(":userid", $_SESSION["id"]);
                    $sql->execute();
                    $row = $sql->fetch(PDO::FETCH_ASSOC);

                    $cashinhand = number_format($row["cash"], 2, ".", "");

                    //calculate total cash required for purchase
                    $purchase_price = $symbol_price * $quantity;

                    if($purchase_price <= $cashinhand)
                    {
                        /*make the purchase*/
                        //get the company name
                        $company_name;
                        $sql= $dbh->prepare("SELECT name FROM companies WHERE symbol = :symbol");
                        $sql->bindValue(":symbol", $symbol);
                        $sql->execute();
                        $row = $sql->fetch(PDO::FETCH_ASSOC);

                        //if name is there in db
                        if($row)
                        {
                            $company_name = $row["name"];
                        }
                        else
                        {
                            //empty string
                            $company_name = "";
                        }

                        //user's new cash after purchase
                        $newcash = $cashinhand - $purchase_price;

                        //insering into table via transactions
                        $dbh->beginTransaction();
                        $sql = $dbh->prepare("INSERT INTO portfolios (user_id, symbol, company, shares) VALUES (:userid, :symbol, :company, :shares) ON DUPLICATE KEY UPDATE shares = shares + :shares");
                        $sql->bindValue(":userid", $_SESSION["id"]);
                        $sql->bindValue(":symbol", $symbol);
                        $sql->bindValue(":company", $company_name);
                        $sql->bindValue(":shares", $quantity);
                        $sql->execute();

                        //updating users cash
                        $sql= $dbh->prepare("UPDATE users SET cash = :newcash WHERE id = :id");
                        $sql->bindValue("newcash", $newcash);
                        $sql->bindValue(":id", $_SESSION["id"]);
                        $sql->execute();


                        //inserting into history table
                        $sql= $dbh->prepare("INSERT INTO history (user_id, action, date_time, symbol, company, shares, price, total) VALUES (:userid, \"BUY\", Now(), :symbol, :company, :shares, :price, :total)");
                        $sql->bindValue(":userid", $_SESSION["id"]);
                        $sql->bindValue(":symbol", $symbol);
                        $sql->bindValue(":company", $company_name);
                        $sql->bindValue(":shares", $quantity);
                        $sql->bindValue(":price", $symbol_price);
                        $sql->bindValue(":total",$purchase_price);
                        $sql->execute();

                        //end transaction
                        $dbh->commit();

                        //redirecting to homepage
                        redirect("index.php");
                    }
                    else
                    {
                        //display error of insufficient funds
                        $flag = true;
                        $msg = "Insufficient funds to buy!";

                        render2("buy_form.php", ["validationerror" => $flag, "message" => $msg]);
                    }
                }
            }

        }
    }
?>
