<?php
    $url = "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol={$_GET["symbol"]}&apikey=INF9154MRHQTFELA&datatype=csv";

    //opening the csv file
    $handle = fopen($url, "r");

    if($handle == false)
    {
        header("Content-type: application/json");
        print(json_encode("could not open file"));
    }
    else
    {
        $rows = [];
        $counter = 0;
        while($data = fgetcsv($handle, ","))
        {
            /*to skip the first 2 iteration, according to documentation
            last month starts from $rows[2]*/
            if($counter < 2)
            {
                $counter += 1;
                continue;
            }
            array_push($rows, $data);
            $counter += 1;

            //to stop after getting 12 months to decrease server load
            if($counter == 14 )
            {
                break;
            }

        }

        $jsontosend = [];

        //sending as a json $row[0] is date and $row[4] is closing price on that date
        foreach($rows as $row)
        {
            $jsontosend [] = ["date" => $row[0], "price" => number_format($row[4], 2, ".", "")];
        }

        header("Content-type: application/json");
        print(json_encode($jsontosend));
    }
?>
